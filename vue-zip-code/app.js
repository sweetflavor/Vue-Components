var app = new Vue({
  el: '#app',
  data: {
    startingZip: '',
    startingCity: '',
    isActive: true
  },
  watch: {
    startingZip: function() {
      this.startingCity = ''
      if (this.startingZip.length == 5 ) {
        this.lookupStartingZip()
      }
    }
  },
  methods: {
    lookupStartingZip: _.debounce(function functionName() {
      var the = this
      the.startingCity = 'Searching ...'
      axios.get('http://ziptasticapi.com/' + the.startingZip)
        .then(function(response) {
          the.startingCity = response.data.city + ', ' + response.data.state
          // the.isActive = true
        })
        .catch(function (error) {
          the.startingCity = "Invalid Zipcode"
        })
    }, 500)
  }
})
